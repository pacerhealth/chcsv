Pod::Spec.new do |s|
  s.name         = "chcsv"
  s.version      = "0.0.1"
  s.summary      = "chcsv."
  s.description  = "chcsv  "
  s.homepage     = "http://www.pacer.cc"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "Ray" => "guolei@me.com" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://daocaoren@bitbucket.org/daocaoren/chcsv.git", :tag => "0.0.1" }
  s.source_files  = "*.{h,m}"
  s.requires_arc = false
end
